import requests
from bs4 import BeautifulSoup
import tkinter as tk
from tkinter import *
from tkinter import scrolledtext
from tkhtmlview import HTMLLabel
from markdown2 import Markdown

def search():
    what_poem_is_about = entry.get()
    HOST = "https://processing.ruscorpora.ru"
    long_link = "https://processing.ruscorpora.ru/search.xml?env=alpha&api=1.0&mycorp=&mysent=&mysize=&mysentsize=&dpp=&spp=&spd=&ct=&mydocsize=&mode=poetic&lang=ru&sort=i_grtagging&nodia=1&text=lexform&ext=10&req="
    URL = (''.join(long_link + what_poem_is_about))

    def get_html(url, params=''):
        r = requests.get(url)
        return r

    def get_content(html):
        soup = BeautifulSoup(html, 'html.parser')
        soup = str(soup)
        soup = soup.replace('<br>', ' | ').replace('<br/>', ' | ').replace('</br>', ' | ')
        soup = BeautifulSoup(soup, 'html.parser')
        poems = soup.find('ol')
    
        cards = []

        for poem in poems:
        
            cards.append({
                'snippet':poem.find('ul').get_text().replace('| [', '[').replace('\xa0', '').replace('\n', '').replace('  ', '').replace('[омонимия не снята]', '').replace('←…→', '').replace('|', '<br>'),
                #'link':'<a href=>"' + HOST + poem.find('a', class_="b-kwic-expl").get('href') + '">Ссылка</a>'
            })
        return cards

    html = get_html(URL)  
    get_content(html.text)
    dictionaries_list = get_content(html.text)
    results_here.delete(1.0, tk.END)
    result_1 = dictionaries_list[0]
    for values in result_1.values():
        results_here.insert(tk.END, values + "<br>")
    result_2 = dictionaries_list[1]
    for values in result_2.values():
        results_here.insert(tk.END, values + "<br>")
    result_3 = dictionaries_list[2]
    for values in result_3.values():
        results_here.insert(tk.END, values + "<br>")
    result_4 = dictionaries_list[3]
    for values in result_4.values():
        results_here.insert(tk.END, values + "<br>")
    result_5 = dictionaries_list[4]
    for values in result_5.values():
        results_here.insert(tk.END, values + "<br>")
    result_6 = dictionaries_list[5]
    for values in result_6.values():
        results_here.insert(tk.END, values + "<br>")
    result_7 = dictionaries_list[6]
    for values in result_7.values():
        results_here.insert(tk.END, values + "<br>")
    result_8 = dictionaries_list[7]
    for values in result_8.values():
        results_here.insert(tk.END, values + "<br>")
    result_9 = dictionaries_list[8]
    for values in result_9.values():
        results_here.insert(tk.END, values + "<br>")
    result_10 = dictionaries_list[9]
    for values in result_10.values():
        results_here.insert(tk.END, values + "<br>")

def show_me_what_you_ve_got(event):
    results_here.edit_modified(0)
    md2html = Markdown()
    results.set_html(md2html.convert(results_here.get(1.0, tk.END)))
    markdownText = results_here.get(1.0, tk.END)
    html2 = md2html.convert(markdownText)
    results.set_html(html2)

def need_more():
    HOST = "https://processing.ruscorpora.ru"
    global p
    page_number = p + 1
    page_number = str(page_number)
    what_poem_is_about = entry.get()
    URL = (''.join("https://processing.ruscorpora.ru/search.xml?lang=ru&mode=poetic&nodia=1&p=" + page_number + "&req=" + what_poem_is_about + "&sort=i_grstd&text=lexform"))

    def get_html(url, params=''):
        r = requests.get(url)
        return r

    def get_content(html):
        soup = BeautifulSoup(html, 'html.parser')
        soup = str(soup)
        soup = soup.replace('<br>', ' | ').replace('<br/>', ' | ').replace('</br>', ' | ')
        soup = BeautifulSoup(soup, 'html.parser')
        poems = soup.find('ol')
    
        cards = []

        for poem in poems:
        
            cards.append({
                'snippet':poem.find('ul').get_text().replace('| [', '[').replace('\xa0', '').replace('\n', '').replace('  ', '').replace('[омонимия не снята]', '').replace('←…→', ''),
                #'link':HOST + poem.find('a', class_="b-kwic-expl").get('href')
            })
        return cards

    html = get_html(URL)  
    get_content(html.text)
    dictionaries_list = get_content(html.text)
    result_1 = dictionaries_list[0]
    for values in result_1.values():
        results_here.insert(tk.END, values + "\n")
    result_2 = dictionaries_list[1]
    for values in result_2.values():
        results_here.insert(tk.END, values + "\n")
    result_3 = dictionaries_list[2]
    for values in result_3.values():
        results_here.insert(tk.END, values + "\n")
    result_4 = dictionaries_list[3]
    for values in result_4.values():
        results_here.insert(tk.END, values + "\n")
    result_5 = dictionaries_list[4]
    for values in result_5.values():
        results_here.insert(tk.END, values + "\n")
    result_6 = dictionaries_list[5]
    for values in result_6.values():
        results_here.insert(tk.END, values + "\n")
    result_7 = dictionaries_list[6]
    for values in result_7.values():
        results_here.insert(tk.END, values + "\n")
    result_8 = dictionaries_list[7]
    for values in result_8.values():
        results_here.insert(tk.END, values + "\n")
    result_9 = dictionaries_list[8]
    for values in result_9.values():
        results_here.insert(tk.END, values + "\n")
    result_10 = dictionaries_list[9]
    for values in result_10.values():
        results_here.insert(tk.END, values + "\n")

    p = int(page_number)
    return p

p = 0

#Interface
window = tk.Tk()
window.title("RU_Poetry_Search")
lbl = Label(window, text="О чем ищем стихи?")  
lbl.pack(side=TOP) 
entry = Entry(window,width=30)
entry.pack(side=TOP) 
entry.focus()
btn_search = Button(window, text="Поиск", command=search)
btn_search.pack(side=TOP) 
btn_more = Button(window, text="Еще", command=need_more)
btn_more.pack(side=BOTTOM)
results_here = Text(window)
#results_here.pack(side=LEFT)
results = HTMLLabel(window, html="") 
results.pack(side=RIGHT)
results_here.bind("<<Modified>>", show_me_what_you_ve_got)
window.mainloop()
